<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\DepartementRepository;
use App\Entity\Departement;

use App\Form\DepartementFormType;

class DepartementController extends AbstractController
{
    /**
     * @Route("/departement", name="departement")
     */
    public function index(DepartementRepository $departmentRepository): Response
    {

        $departements = $departmentRepository->findall();
        return $this->render('departement/index.html.twig', [
            'departements' => $departements,
        ]);
    }

     /**
     * @Route("/staff/departement/gestion/{slug?}",name="departement_gestion")
     */
    public function gestion(Request $request,DepartementRepository $departmentRepository): Response
    {
    
        $departement = new Departement();
        //Récupérations de la catégorie d'origine
        if($request->get('slug') != null){
            $slug = $request->get('slug');
            $departement = $departmentRepository->findOneBy(['slug' => $slug]);
        }

        $form = $this->createForm(DepartementFormType::class,$departement);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //Karen : Amenez-moi le manager!
            $em = $this->getDoctrine()->getManager();
            $em->persist($departement);
            $em->flush();

            $this->addFlash('Success','Departement ajouter');
            return $this->redirectToRoute('departement');
        }

        return $this->render('departement/gestion.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("admin/departement/delete/{slug}",name="departement_delete")
     */
    public function delete(Request $request,DepartementRepository $departmentRepository): RedirectResponse 
    {
        $departementEntity = $departmentRepository->findOneBy(['slug'=>$request->get('slug')]);
        
        if(!$departementEntity){
            $this->addFlash("danger",'Departement non trouvé');
            return $this->redirectToRoute('departement');
        }

        //Encore Karen, au-secours !!!!
        $em = $this->getDoctrine()->getManager();
        $em->remove($departementEntity);
        $em->flush();

        $this->addFlash("warning",'Departement supprimé !');
        return $this->redirectToRoute('departement');
    }





}
