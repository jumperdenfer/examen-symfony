<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\VilleRepository;

use App\Entity\Ville;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(VilleRepository $villeRepository): Response
    {
        $villeList = $villeRepository->findall();

        return $this->render('home/index.html.twig', [
            'villelist' => $villeList,
        ]);
    }
}
