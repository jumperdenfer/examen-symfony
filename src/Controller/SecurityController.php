<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use App\Entity\User;
use App\Form\UserFormType;
use App\Repository\UserRepository;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/admin/user/gestion/{id?}",name="user_register")
     */
    public function gestion(EntityManagerInterface $em,Request $request,UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();

        if($request->get('id') != null){
            $userRepository = $this->getDoctrine()->getRepository(User::class);
            $id = $request->get('id');
            $user = $userRepository->find($id);
        }

        $form = $this->createForm(UserFormType::class,$user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $roles = $form->get('roles')->getData();
            $user->setRoles([0 => $roles]);

            if($form['password']->getData() != null){
                $plainPassword = $form['password']->getData();
                if (trim($plainPassword) != '') {
                    //encrypt pass
                    $password = $passwordEncoder->encodePassword($user, $plainPassword);
                    $user->setPassword($password);
                } else {
                    $passError = new FormError("Age must be greater than 18");
                    $form->get('password')->addError($passError);
                }
            }

            $em->persist($user);
            $em->flush();
            $this->addFlash('success','User Added');
        }

        return $this->render('security/gestion.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/staff/user",name="list_user")
     */
    public function userlist(UserRepository $userRepository)
    {

        $users = $userRepository->findall();

        return $this->render('security/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/user/delete/{id}",name="user_delete")
     */
    public function userDelete(Request $request,UserRepository $userRepository) : RedirectResponse 
    {
        $userEntity = $userRepository->findOneBy(['id'=>$request->get('id')]);
        
        $loggedUser = $this->getUser();
        if($loggedUser->getId() === $userEntity->getId()){
            $this->addFlash("warning",'Vous ne pouvez pas vous supprimez !');
            return $this->redirectToRoute('list_user');
        }

        if(!$userEntity){
            $this->addFlash("danger",'Utilisateur non trouvée');
            return $this->redirectToRoute('list_user');
        }

        //Encore Karen, au-secours !!!!
        $em = $this->getDoctrine()->getManager();
        $em->remove($userEntity);
        $em->flush();

        $this->addFlash("warning",'Utilisateur supprimée !');
        return $this->redirectToRoute('list_user');
    }
}
