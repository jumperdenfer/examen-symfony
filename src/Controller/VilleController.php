<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\VilleRepository;

use App\Form\VilleFormType;
use App\Entity\Ville;


class VilleController extends AbstractController
{
    /**
     * @Route("/ville", name="ville")
     */
    public function index(VilleRepository $villeRepository): Response
    {
        $villeList = $villeRepository->findall();

        return $this->render('ville/index.html.twig', [
            'villelist' => $villeList
        ]);
    }

    /**
     * @Route("/staff/ville/gestion/{slug?}",name="ville_gestion")
     */
    public function gestion(Request $request,VilleRepository $villeRepository): Response
    {
    
        $ville = new Ville();
        //Récupérations de la catégorie d'origine
        if($request->get('slug') != null){
            $slug = $request->get('slug');
            $ville = $villeRepository->findOneBy(['slug' => $slug]);
        }

        $form = $this->createForm(VilleFormType::class,$ville);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //Karen : Amenez-moi le manager!
            $em = $this->getDoctrine()->getManager();
            $em->persist($ville);
            $em->flush();

            $this->addFlash('Success','Ville ajouter');
            return $this->redirectToRoute('ville');
        }

        return $this->render('ville/gestion.html.twig',[
            'form' => $form->createView()
        ]);
    }

       /**
     * @Route("admin/ville/delete/{slug}",name="ville_delete")
     */
    public function delete(Request $request,VilleRepository $villeRepository): RedirectResponse 
    {
        $villeEntity = $villeRepository->findOneBy(['slug'=>$request->get('slug')]);
        
        if(!$villeEntity){
            $this->addFlash("danger",'Ville non trouvée');
            return $this->redirectToRoute('ville');
        }

        //Encore Karen, au-secours !!!!
        $em = $this->getDoctrine()->getManager();
        $em->remove($villeEntity);
        $em->flush();

        $this->addFlash("warning",'Ville supprimée !');
        return $this->redirectToRoute('ville');
    }

}
